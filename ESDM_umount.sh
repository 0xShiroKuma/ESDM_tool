#!/bin/bash

choose_device ()
{
	device=`df | grep -E '^/dev/(sd[b-d][1-9])|(mmcblk0p1)' | cut -d \  -f 1`
	echo -e "== Devices avalables == \n"
	nbr=0
	for i in $device
		do
			let "nbr+=1"
			echo $nbr "- "$i 
	done				
	echo -e "$device_list\n==> Choose the device to umount :"
	read -p "==> " choise
	
	nbr=0
	for i in $device
		do
			let "nbr+=1"
			if [ $nbr -eq $choise ]
				then
					break
				else
				continue
		fi
		done
		echo -e "\n"
		sudo umount $device && echo -e "\n$device umounted... !" || echo -e "\nError... Please check information(s) given"

}

echo -e "					     .~."
echo -e "  _   ,--()	====  USB umount tool ====   /V\ "
echo  "( )-'-.------|>	==========================  // \\\\ "
echo -e "       \`--[]	===== By 0xShiroKuma ===== /(   )\ "
echo -e " 	    	       			    ^\`~'^\n"

if [ -e /tmp/.device_mounted ]
	then
		device=`head -n 1 /tmp/.device_mounted`
		echo -e "==> Umount $device ? [Y/n]"
		read -p '==> ' choise
		case $choise in
			'Y'|'y')
		sudo umount $device && echo -e "\n$device umounted... !" || echo -e "\nError... Please check information(s) given"
				;;
			*)
				choose_device
				
		esac
	else
		choose_device
fi